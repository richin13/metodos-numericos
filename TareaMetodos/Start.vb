﻿Module Start

    Sub Main()
        Dim choice = "0"
        Do While choice <> "9"
            Console.WriteLine(vbCrLf + vbCrLf)
            Console.WriteLine("Tarea Métodos numéricos")
            Console.WriteLine("=======================")
            Console.WriteLine(" 1. Bisección")
            Console.WriteLine(" 2. Punto fijo")
            Console.WriteLine(" 3. Método de Newton")
            Console.WriteLine(" 9. Salir")
            Console.WriteLine("-----------------------")
            Console.Write("Seleccione una opción: ")

            choice = Console.ReadLine()

            If choice = "1" Then
                Console.WriteLine("==>Bisección<==")
                Solver(New BisectionMethod())
            ElseIf choice = "2" Then
                Console.WriteLine("==>Punto fijo<==")
                Solver(New FixedPointMethod())
            ElseIf choice = "3" Then
                Console.WriteLine("==>Newton<==")
                Solver(New NewtonMethod())
            ElseIf choice = "9" Then
                Exit Sub
            Else
                Console.WriteLine("(w) Seleccione una opción válida")
            End If
            Threading.Thread.Sleep(2000)
        Loop
    End Sub

    Function Solver(method As AbstractMethod)
        If method.GetData() = 0 Then
            Console.WriteLine(method.Solve())
        End If

        Return 0
    End Function

End Module
