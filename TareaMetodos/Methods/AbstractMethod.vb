﻿Imports MPFunction = org.mariuszgromada.math.mxparser.Function

Public Interface AbstractMethod
    Property fx As MPFunction
    Property iterations As Integer
    Property errorTolerance As Double

    Function GetData()
    Function Solve() As String

End Interface
