﻿Imports org.mariuszgromada.math.mxparser
Imports TareaMetodos

Public MustInherit Class ConcreteMethod
    Implements AbstractMethod

    Public Property errorTolerance As Double Implements AbstractMethod.errorTolerance

    Public Property fx As [Function] Implements AbstractMethod.fx

    Public Property iterations As Integer Implements AbstractMethod.iterations

    Public Sub New()
        ' Empty on purpose
    End Sub

    Public Sub New(fx, iterations, errorTolerance)
        Me.fx = fx
        Me.iterations = iterations
        Me.errorTolerance = errorTolerance
    End Sub

    Public MustOverride Function GetData() As Object Implements AbstractMethod.GetData

    Public MustOverride Function Solve() As String Implements AbstractMethod.Solve

    Protected Function f(x As Double) As Double
        Return fx.calculate(x)
    End Function
End Class
