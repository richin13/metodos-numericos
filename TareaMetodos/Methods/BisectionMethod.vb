﻿Imports org.mariuszgromada.math.mxparser
Imports TareaMetodos

Public Class BisectionMethod
    Inherits ConcreteMethod

    Private Property intervalMin As Double

    Private Property intervalMax As Double

    Public Sub New()
        ' Empty on purpose
    End Sub

    Public Sub New(fx, iterations, errorTol, a, b)
        MyBase.New(fx, iterations, errorTol)
        intervalMin = a
        intervalMax = b
    End Sub

    Public Overrides Function GetData() As Object
        Console.Write("Ingrese el criterio f(x) de la función: f(x)=")
        Dim rfx = Console.ReadLine()
        fx = New [Function]("f", rfx, "x")

        Try
            Console.Write("Ingrese el valor a del intérvalo [a, b]: ")
            intervalMin = Convert.ToDouble(Console.ReadLine())

            Console.Write("Ingrese el valor b del intérvalo [a, b]: ")
            intervalMax = Convert.ToDouble(Console.ReadLine())

            Console.Write("Ingrese el número máximo de iteraciones: ")
            iterations = Convert.ToInt32(Console.ReadLine())

            Console.Write("Ingrese la tolerancia de error: ")
            errorTolerance = Convert.ToDouble(Console.ReadLine())

            Return 0
        Catch ex As FormatException
            Console.WriteLine("(e) Formato inválido.")
            Return -1
        End Try
    End Function

    Public Overrides Function Solve() As String
        Dim i As Integer = 0
        Dim a As Double = intervalMin
        Dim b As Double = intervalMax
        Dim FA = f(a)
        Dim p As Double

        While i < iterations
            p = a + ((b - a) / 2)
            Dim FP = f(p)
            If FP = 0 Or ((b - a) / 2) < errorTolerance Then
                Return String.Format("(i) Solución después de {0} iteraciones => {1}", i, FP)
            End If
            i += 1
            If FA * FP > 0 Then
                a = p
                FA = FP
            Else
                b = p
            End If
        End While
        Return String.Format("(i) El método fracasó después de {0} iteraciones", iterations)
    End Function
End Class
