﻿Imports System.Math
Imports org.mariuszgromada.math.mxparser

Public Class NewtonMethod
    Inherits ConcreteMethod

    Private pInitial As Double

    Public Sub New()
        ' Empty on purpose
    End Sub

    Public Sub New(fx, iterations, errorTol, p0)
        MyBase.New(fx, iterations, errorTol)
        pInitial = p0
    End Sub

    Public Overrides Function GetData() As Object
        Console.Write("Ingrese el criterio f(x) de la función: f(x)=")
        Dim rfx = Console.ReadLine()
        fx = New [Function]("f", rfx, "x")

        Try
            Console.Write("Ingrese el valor de la aproximación inicial p0: ")
            pInitial = Convert.ToDouble(Console.ReadLine())

            Console.Write("Ingrese el número máximo de iteraciones: ")
            iterations = Convert.ToInt32(Console.ReadLine())

            Console.Write("Ingrese la tolerancia de error: ")
            errorTolerance = Convert.ToDouble(Console.ReadLine())

            Return 0
        Catch ex As FormatException
            Console.WriteLine("(e) Formato inválido.")
            Return -1
        End Try
    End Function

    Public Overrides Function Solve() As String
        Dim i As Integer = 0
        Dim x0 = pInitial
        While i < iterations
            Dim y0 = f(x0)
            Dim yp = fPrime(x0)

            Dim x1 = x0 - y0 / yp
            Dim p = f(x1)

            If Abs(p) < errorTolerance Then
                Return String.Format("(i) Solución después de {0} iteraciones => {1}", i, p)
            End If
            x0 = x1
            i += 1
        End While

        Return String.Format("(i) El método fracasó después de {0} iteraciones", iterations)
    End Function

    Private Function fPrime(xx As Double) As Double
        Dim x As Argument = New Argument("x", xx)
        Dim e As Expression = New Expression("der(f(x), x)", x, fx)
        Return e.calculate()
    End Function
End Class
