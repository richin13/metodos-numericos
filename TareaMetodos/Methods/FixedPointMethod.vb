﻿Imports System.Math
Imports org.mariuszgromada.math.mxparser
Imports TareaMetodos

Public Class FixedPointMethod
    Inherits ConcreteMethod

    Private pInitial As Double

    Public Sub New()
        ' Empty on purpose
    End Sub

    Public Sub New(fx, iterations, errorTol, p0)
        MyBase.New(fx, iterations, errorTol)
        pInitial = p0
    End Sub

    Public Overrides Function GetData() As Object
        Console.Write("Ingrese el criterio g(x) de la función: f(x)=")
        Dim rfx = Console.ReadLine()
        fx = New [Function]("f", rfx, "x")

        Try
            Console.Write("Ingrese el valor de la aproximación inicial p0: ")
            pInitial = Convert.ToDouble(Console.ReadLine())

            Console.Write("Ingrese el número máximo de iteraciones: ")
            iterations = Convert.ToInt32(Console.ReadLine())

            Console.Write("Ingrese la tolerancia de error: ")
            errorTolerance = Convert.ToDouble(Console.ReadLine())

            Return 0
        Catch ex As FormatException
            Console.WriteLine("(e) Formato inválido.")
            Return -1
        End Try
    End Function

    Public Overrides Function Solve() As String
        Dim i As Integer = 0
        Dim p As Double
        Dim p0 As Double = pInitial

        While i < iterations
            p = f(p0)

            If Abs(p - p0) < errorTolerance Then
                Return String.Format("(i) Solución después de {0} iteraciones => {1}", i, p)
            End If
            i += 1
            p0 = p
            Console.WriteLine(p)
        End While
        Return String.Format("(i) El método fracasó después de {0} iteraciones", iterations)
    End Function
End Class
